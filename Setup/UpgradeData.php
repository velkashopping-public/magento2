<?php

namespace Velkashopping\Magento2\Setup;

use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\DB\Ddl\Table;

/**
 * @codeCoverageIgnore
 */
class UpgradeData implements UpgradeDataInterface
{
    /**
     * @var \Magento\Cms\Model\PageFactory
     */
    protected $_pageFactory;

    /**
     * Construct
     *
     * @param \Magento\Cms\Model\PageFactory $pageFactory
     */
    public function __construct(\Magento\Cms\Model\PageFactory $pageFactory)
    {
        $this->_pageFactory = $pageFactory;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function upgrade(
        ModuleDataSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $setup->startSetup();

        if (version_compare($context->getVersion(), "2.0.1") < 0) {
            $connection = $setup->getConnection();
            $connection->addColumn($setup->getTable("sales_order"), "vid", [
                "type" => Table::TYPE_TEXT,
                "length" => 255,
                "nullable" => true,
                "default" => "0",
                "comment" => "1=> vid",
            ]);

            $connection->addColumn(
                $setup->getTable("sales_order"),
                "Sent_to_velka",
                [
                    "type" => Table::TYPE_INTEGER,
                    "length" => 11,
                    "nullable" => true,
                    "default" => "0",
                    "comment" => "1=> Sent_to_velka",
                ]
            );
        }

        $setup->endSetup();
    }
}
