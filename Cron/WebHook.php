<?php
namespace Velkashopping\Magento2\Cron;

class WebHook
{
    const CURL_URL = "https://velkashopping.com/api/webhooks/track";

    public function __construct(
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\Framework\HTTP\Client\Curl $curl
    ) {
        $this->resource = $resource;
        $this->connection = $resource->getConnection();
        $this->_curl = $curl;
    }

    public function execute()
    {
        $writer = new \Zend_Log_Writer_Stream(BP . "/var/log/velka-webhook.log");
        $logger = new \Zend_Log();
        $logger->addWriter($writer);

        $sales_order_table = $this->resource->getTableName("sales_order");

        $sales_order_sql =
            "SELECT * FROM `" .
            $sales_order_table .
            "` WHERE `vid` !='0' AND `Sent_to_velka`='0'";

        $sales_order_result = $this->connection->fetchAll($sales_order_sql);

        foreach ($sales_order_result as $sales_order) {
            try {
                $postData = [
                    "click_id" => $sales_order["vid"],
                    "order_id" => $sales_order["increment_id"],
                    "order_total" =>
                        $sales_order["grand_total"] -
                        $sales_order["tax_amount"] -
                        $sales_order["shipping_amount"],
                ];

                $this->_curl->get(self::CURL_URL);
                $this->_curl->post(self::CURL_URL, $postData);
                $response = $this->_curl->getBody();

                $sales_order_update_sql =
                    "UPDATE `" .
                    $sales_order_table .
                    "` SET `Sent_to_velka` = '1' WHERE `increment_id`='" .
                    $sales_order["increment_id"] .
                    "'";
                $this->connection->query($sales_order_update_sql);

            } catch (\Exception $e) {
                $logger->info($e->getMessage());
            }
        }

        return $this;
    }
}
