<?php
namespace Velkashopping\Magento2\Controller\V1;

class Ping extends \Magento\Framework\App\Action\Action
{
    protected $_pageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \Magento\Framework\Controller\ResultFactory $resultFactory,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\Framework\ObjectManagerInterface $objectmanager
    ) {
        $this->_pageFactory = $pageFactory;
        $this->resultFactory = $resultFactory;
        $this->orderFactory = $orderFactory;
        $this->resource = $resource;
        $this->connection = $resource->getConnection();
        $this->objectManager = $objectmanager;
        return parent::__construct($context);
    }

    public function execute()
    {
        $productMetadata = $this->objectManager->get(
            "\Magento\Framework\App\ProductMetadataInterface"
        );
        $mversion = $productMetadata->getVersion();
        $moduleVersion = $this->objectManager
            ->get("\Magento\Framework\Module\ResourceInterface")
            ->getDbVersion("Velkashopping_Magento2");

        $data = [
            "status" => 200,
            "velkashopping_version" => isset($moduleVersion)
                ? $moduleVersion
                : "",
            "magento_version" => $mversion,
        ];

        $this->getResponse()
            ->clearHeaders()
            ->setHeader("Content-type", "application/json", true);
        $this->getResponse()->setBody(json_encode($data));
    }
}
